/*************************************************************************
 * This project entails the sorting of large sets of images (in jpg format)
 * based on their colour information and using different sorting algorithms.
 * For each image in a directory, you have to calculate a feature vector
 * based on its colour information. The feature vector must store the
 * mean of each of the colour channels (RGB values). The feature vectors
 * must then be sorted according to the specified parameter (their mean R,
 * G or B values). The sorted feature vectors must then be used to display
 * the images in the sorted order in the GUI of your program. The GUI
 * must also enable the user to re-sort the images using any of the
 * sorting algorithms and according to any of the parameters
 *, after which the image display must be updated. For script marking
 *purposes, your program must also output a text file with the names
 * of the files in colour-sorted order.
 *
 *
 *  Compilation:  javac ImageSort.java
 *  Execution:    java ImageSort <imagedirectory> <algorithm><color><gui>
 *  Dependencies: ImageObject.java Selection.java Merge.java
 *  Data files:   http://www.cs.sun.ac.za/rw214/projects/Proj1.html
 *  @author 17332001
 *************************************************************************/

import java.io.*;

import javax.swing.JOptionPane;
public class ImageSort {

	/**
	 * @author 17332001
	 * @throws IOException 
	 */

	public static void work(String imagedir,int algorithm, int color, int gui) throws IOException{
		int imagenumber,greenSum,redSum,blueSum;
		String data="";
		long startTime,endTime,duration;//



		File folder = new File(imagedir);
		File[] listOfFiles = folder.listFiles();
		imagenumber=listOfFiles.length;
		ImageObject imageArray []= new ImageObject[imagenumber];

		/**
		 * Scanning through image directory, calculating image average vector and making image objects
		 */
		for (int i = 0; i < imagenumber; i++) {
			greenSum=0;
			redSum=0;
			blueSum=0;
			if (listOfFiles[i].isFile()) {
				Picture my = new Picture("./"+imagedir+"/"+listOfFiles[i].getName());
				for(int x=0;x<my.height();x++) {
					for(int y=0;y<my.width();y++) {
						redSum = my.get(x, y).getRed()+redSum;
						greenSum = my.get(x, y).getGreen() + greenSum;
						blueSum = my.get(x, y).getBlue()+blueSum;
					}
				}
				imageArray[i] = new ImageObject(listOfFiles[i].getName(),redSum/(my.width()*my.height()),
						greenSum/(my.width()*my.height()),blueSum/(my.width()*my.height()));
				imageArray[i].setSortColor(color);
			}
		}
		/**
		 * Sorting images using a specified algorithm and the color
		 */
		startTime= System.currentTimeMillis();
		if(algorithm==0) {Insertion.sort(imageArray);}
		else if(algorithm==1) {Shell.sort(imageArray);}
		else if(algorithm==2) {Merge.sort(imageArray);}
		else if(algorithm==3) {Quick.sort(imageArray);}
		else if(algorithm==4) {Selection.sort(imageArray);}
		else {Quick.sort(imageArray);}
		endTime= System.currentTimeMillis();
		duration = endTime-startTime;

		for(int ha =0;ha<imageArray.length;ha++){
			data = data + imageArray[ha].getName() + '\n';
		}
		/**
		 * Write to a file
		 */
		File file = new File("sorted.txt");
		file.createNewFile();
		FileWriter writer = new FileWriter(file);
		writer.write(data);
		writer.flush();
		writer.close();	

		if(gui==1){
			gui(String.valueOf(duration),imagedir,color);
		}
	}
	public static void gui(String data, String imagedir ,int color) throws IOException{
		long startTime,endTime,duration;
		String [] methods ={"insertion", "shell", "merge", "quick","selection"};
		Object[] options = {"Unsort","Compare"};

		int option = JOptionPane.showOptionDialog(null,
				"It took : "+data,
				null,
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				null,
				options,
				options[1]);

		if (option == JOptionPane.OK_OPTION) {
			File folder = new File(imagedir);
			File[] listOfFiles = folder.listFiles();

			for (int i = 0; i < listOfFiles.length; i++) {
				//System.out.println(listOfFiles[i].getName());
				data = data + listOfFiles[i].getName() + '\n';	
			}

			File file = new File("unsorted.txt");
			file.createNewFile();
			FileWriter writer = new FileWriter(file);
			writer.write(data);
			writer.flush();
			writer.close();	
			JOptionPane.showMessageDialog(null, "The data was unsort!");
		}
		else if (option == JOptionPane.NO_OPTION){
			String cmpre ="";
			for(int i=0;i<5;i++){
				startTime = System.currentTimeMillis();
				work(imagedir,i,color,0);
				endTime = System.currentTimeMillis();
				duration=endTime -startTime;		
				cmpre = cmpre + methods[i]  +"  " + String.valueOf(duration)+ '\n';
			}
			JOptionPane.showMessageDialog(null,cmpre);
		}
	}

	public static void main(String[] args) throws IOException {
		work(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]) , Integer.parseInt(args[3]));
	}
}
