import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ImageSortTest {


	@Test
	public void test1() throws IOException {

		ImageSort jon = new ImageSort ();
		jon.work("images", 0, 0, 0);
	}
	@Test
	public void test2() throws IOException {
		ImageSort test2 = new ImageSort ();
		test2.work("images", 1, 1, 0);	
	}
	@Test
	public void test3() throws IOException {
		ImageSort test3 = new ImageSort ();
		test3.work("images", 2, 0, 0);	
	}
	@Test
	public void test4() throws IOException {
		ImageSort test4 = new ImageSort ();
		test4.work("images", 3, 2, 0);	
	}
	@Test
	public void test5() throws IOException {
		ImageSort test5 = new ImageSort ();
		test5.work("images", 4, 0, 1);	
	}
	@Test
	public void test6() throws IOException {
		ImageSort test6 = new ImageSort ();
		test6.work("images", 14, 0, 1);	
	}
}
