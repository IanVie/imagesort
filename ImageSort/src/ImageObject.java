/*************************************************************************
 *  Compilation:  javac ImageObject.java
 *  Execution:    java ImageObject
 *  Dependencies: StdOut.java StdIn.java
 *  @author 17332001
 *************************************************************************/

public class ImageObject  implements Comparable<ImageObject>{
	private String name;
	private int red;
	private int green;
	private int blue;
	public int sortColor= 0;

	/**
	 * Construvctor of image object that consist:
	 * @param name, the name of the image
	 * @param red, the average number of red value in the image
	 * @param green, the average number of red value in the image
	 * @param blue, the average number of red value in the image
	 */

	public ImageObject(String name, int red, int green,int blue) {
		this.name =name;
		this.red =red;
		this.green =green;
		this.blue=blue;
	}

	public String getName() {
		return name;
	}
	/**
	 * Color to sort image based on from client
	 * @param sortColor sets the value of the
	 */
	public void setSortColor(int sortColor) {
		this.sortColor=sortColor;
	}

	/**
	 * Generic method that compares two image
	 * @param o, image to be compared to the image object
	 * @return 0 if images are equal
	 */
	@Override
	public int compareTo(ImageObject o) {
		if(sortColor==1){
			if(green==o.green)
				return 0;
			if(green>o.green)
				return 1;
			return -1;

		}else if(sortColor==2){
			if(blue==o.blue)
				return 0;
			if(blue>o.blue)
				return 1;
			return -1;
		} else {
			if(red==o.red)
				return 0;
			if(red>o.red)
				return 1;
			return -1;

		}
	}
}
