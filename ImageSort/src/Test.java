import javax.swing.JOptionPane;

public class Test {

	/**
	 * @param args
	 */
	
	public static void gui(String data){
		Object[] options = {"Unsort","Compare"};
		
		int option = JOptionPane.showOptionDialog(null,
				data,
				null,
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				null,
				options,
				options[1]);

		if (option == JOptionPane.OK_OPTION){
			JOptionPane.showMessageDialog(null, "The data was unsort!");
		}
		else if (option == JOptionPane.NO_OPTION){
			JOptionPane.showMessageDialog(null, "Compare!");
		}
		
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.out.println(args[0]);//directory name
		//System.out.println(args[1]);//algorithm (0-insertion, 1-shell, 2-merge, 3-quick, 4-selection)
		//System.out.println(args[2]);//(0-mean red value, 1-mean green value, 2-mean blue value)
		//System.out.println(args[3]);// GUI of the program should be launched (0-no, 1-yes)

		/*ImageObject ian = new ImageObject("ian",1,2,3);
		System.out.println(ian.getRed());*/

		/*Object[] possibleValues = { "First", "Second", "Third" };
		Object selectedValue = JOptionPane.showInputDialog(null,
		"Choose one", "Input",
		JOptionPane.INFORMATION_MESSAGE, null,
		possibleValues, possibleValues[0]);*/

		gui("Ian Vurayai");



	}

}
