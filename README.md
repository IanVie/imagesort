## READ ME

This is a small file to help you set things up.

Go to [the wiki](http://gitwiki.cs.sun.ac.za) and follow the instructions for undergraduates.

In all, you will need to:
1. If you have no SSH key, create one. (link above)
2. Add your ssh public key to gitlab. (link above)
3. Clone your project to your local machine.
4. Add a file, called "test.md", with nothing in it.
5. Git add, git commit, and git push the file to your repo.

Once you have cloned, added and pushed, you should be done with setting up your repo. You can then continue with your work. Bare in mind that you need to commit regularly.

### Tips
* Use the issue tracker in your repo to help you prioritise and keep track of tasks.
* Write useful commit messages.
* Git is simple but powerful. Read, read and read some more. Google is your friend.
