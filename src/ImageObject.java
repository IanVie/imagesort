
public class ImageObject {
	private String name;
	private int red;
	private int green;
	private int blue;
	
	public ImageObject(String name, int red, int green,int blue){
		this.name =name;
		this.red =red;
		this.green =green;
		this.blue=blue;
		
	}
	
	private String getName(){
		return name;
	}
	public int getRed(){
		return red;
	}
	public int getGreen(){
		return green;
		
	}
	public int getBlue(){
		return blue;
	}
	
	
	public String toString(){
		return name +" "+red+" "+green+" "+blue;
	}

}
